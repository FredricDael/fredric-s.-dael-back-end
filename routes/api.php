<?php

use App\Http\Controllers\peopleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/people/byId/{id}',[peopleController::class, 'getById']);
Route::get('/people/getAll',[peopleController::class, 'getAll']);
Route::post('/people/create',[peopleController::class, 'create']);
Route::get('/people/search',[peopleController::class, 'search']);
Route::get('/people/delete/{id}',[peopleController::class, 'delete']);
Route::post('/people/update',[peopleController::class, 'update']);


