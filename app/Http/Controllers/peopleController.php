<?php

namespace App\Http\Controllers;

use App\Models\people;
use Illuminate\Http\Request;

class peopleController extends Controller
{
    public function getById($id){
        $people = people::where('id' , $id)->first();
        return $people;
    } 

    public function getAll(){
        $people = people::all();
        return $people;
    }

    public function create(Request $request){
        people::create($request->all());
        return 'created';
    }

    public function search(Request $request){
        $people = people::where('first_name' ,'like', "%{$request->search}%")
                        ->orWhere('last_name','like', "%{$request->search}%")
                        ->get();

        return $people;
    }

    public function delete($id){
        people::destroy($id);
        return 'deleted';
    }

    public function update(Request $request){
        $people = people::find($request->id);

        // $people->first_name = $request->first_name;
        // $people->last_name = $request->last_name;
        // $people->date_registered = $request->date_registered;
        // $people->address = $request->address;
        // $people->phone = $request->phone;
        // $people->email = $request->email;

        $input = $request->all();
        $people->fill($input)->save();

        $people->save();

        return 'updated';
    }

    
}
